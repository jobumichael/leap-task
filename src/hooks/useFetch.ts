import axios from "axios";
import { useEffect, useState } from "react";

interface IUseFetch {
  data: any;
  loading: boolean;
  error: boolean;
}

const useFetch = (url: string) => {
  const [state, setState] = useState<IUseFetch>({
    data: null,
    loading: true,
    error: false,
  });

  useEffect(() => {
    const fetchData = async () => {
      try {
        const result = await axios.get(`/api/proxy-middleware?url=${url}`);

        setState((prevState: IUseFetch) => {
          return { ...prevState, data: result.data, loading: false };
        });
      } catch (error) {
        setState((prevState: IUseFetch) => {
          return { error: true, loading: false, data: prevState.data };
        });
      }
    };
    fetchData();
  }, [url]);

  return { ...state };
};

export default useFetch;
