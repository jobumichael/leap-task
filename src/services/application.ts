import axios from "axios";
import { IApplication } from "interfaces";

export const postApplication = async (
  application: IApplication
): Promise<any> => {
  const url = `/api/proxy-middleware?url=applications`;

  const response: any = await axios.post(url, application);
  return response;
};

export const updateApplication = async (
  application: IApplication
): Promise<any> => {
  console.log("🚀 ~ file: application.ts ~ line 16 ~ application", application);
  const url = `/api/proxy-middleware?url=applications/${application.id}`;

  const response: any = await axios.put(url, application);
  return response;
};

export const deleteApplication = async (id: string): Promise<any> => {
  const url = `/api/proxy-middleware?url=applications/${id}`;

  const response: any = await axios.delete(url);
  return response;
};
