import axios from "axios";
import { apiDomain } from "config/apiUrls";
import { IUser } from "interfaces";

export const login = async (user: IUser): Promise<any> => {
  const url = `${apiDomain}/users/login`;
  const response: any = await axios.post(url, user);
  return response;
};

export const register = async (user: IUser): Promise<any> => {
  const url = `${apiDomain}/users`;
  const response: any = await axios.post(url, user);
  return response;
};
