export interface IApplication {
  id: string;
  name?: string;
  lang?: string;
  version?: string;
  secret?: string;
}
