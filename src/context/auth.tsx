import axios from "axios";
import { IUser } from "interfaces";
import { useRouter } from "next/router";
import { destroyCookie, parseCookies, setCookie } from "nookies";
import React, { useContext, useEffect, useState } from "react";

const AuthContext = React.createContext(
  {} as {
    user: any;
    authenticate: (newToken: string) => Promise<void>;
    logout: (redirectLocation?: string) => void;
    isAuthenticated: boolean;
    sessionId: string;
    loading: boolean;
  }
);

export const AuthProvider = ({ children }: any) => {
  const [user, setUser] = useState<IUser | null>(null);
  const [loading, setLoading] = useState(true);
  const router = useRouter();

  const logout = (redirectLocation?: string) => {
    setLoading(true);
    destroyCookie(null, "sessionId");
    setUser(null);
    console.log("Redirecting");
    setLoading(false);
    router.push(redirectLocation || "/auth/login");
  };

  const authenticate = async (sessionId: string) => {
    try {
      setLoading(true);
      setCookie(null, "sessionId", sessionId, {
        maxAge: 30 * 24 * 60 * 60,
        path: "/",
      });
      const apiResponse = await axios.get(`/api/proxy-middleware?url=users`);
      setUser(apiResponse.data);
      router.push("/");
    } catch (error) {
      console.log({ error });
      setUser(null);
      destroyCookie(null, "sessionId");
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    const { sessionId } = parseCookies();

    if (!sessionId) {
      setLoading(false);
      return;
    }

    authenticate(sessionId);
  }, []);

  return (
    <AuthContext.Provider
      value={{
        user,
        authenticate,
        logout,
        isAuthenticated: !!user,
        sessionId: parseCookies().sessionId,
        loading,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => useContext(AuthContext);
