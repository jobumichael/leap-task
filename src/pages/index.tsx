import { PlusOutlined } from "@ant-design/icons";
import { Button } from "antd";
import axios from "axios";
import {
  ApplicationDetail,
  ApplicationList,
  Breadcrumb,
  CreateApplication,
  Layout,
  PageLoader,
} from "components";
import { apiDomain } from "config/apiUrls";
import { useAuth } from "context/auth";
import { getApiOptions } from "helper/api";
import { IApplication } from "interfaces";
import type { GetServerSidePropsContext, NextPage } from "next";
import Head from "next/head";
import nookies from "nookies";
import { useState } from "react";

const Home: NextPage = () => {
  const [pageType, setPageType] = useState("list");
  const [selectedApp, setSelectedApp] = useState<IApplication | null>();

  const { loading } = useAuth();

  const handleCreateApp = () => {
    setSelectedApp(null);
    setPageType("create");
  };

  const getComp = () => {
    switch (pageType) {
      case "detail":
        return (
          <ApplicationDetail
            setPageType={setPageType}
            selectedApp={selectedApp}
          />
        );

      case "list":
        return (
          <ApplicationList
            setPageType={setPageType}
            setSelectedApp={setSelectedApp}
          />
        );
      case "create":
      case "edit":
        return (
          <CreateApplication
            setPageType={setPageType}
            selectedApp={selectedApp}
          />
        );
      default:
        return (
          <ApplicationList
            setPageType={setPageType}
            setSelectedApp={setSelectedApp}
          />
        );
    }
  };

  if (loading) {
    return <PageLoader></PageLoader>;
  }

  return (
    <div>
      <Head>
        <title>Leap task</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Layout>
        <Breadcrumb pageType={pageType} setPageType={setPageType} />

        {pageType !== "create" && (
          <Button
            type="primary"
            icon={<PlusOutlined />}
            onClick={handleCreateApp}
          >
            Create new App
          </Button>
        )}
        <br />
        <br />
        {getComp()}
      </Layout>
    </div>
  );
};

export async function getServerSideProps(ctx: GetServerSidePropsContext) {
  const { sessionId } = nookies.get(ctx);

  if (sessionId) {
    const opt = getApiOptions({
      url: `${apiDomain}/users`,
      sessionId,
    });

    try {
      const apiResponse = await axios.request(opt);
      if (apiResponse.status === 200) {
        return {
          props: {
            user: apiResponse.data,
          },
        };
      }
    } catch (error) {
      console.log(error);
    }
  }

  const { res, resolvedUrl } = ctx;
  res.statusCode = 302;
  res.setHeader(
    "Location",
    `/auth/login?redirect=${encodeURIComponent(resolvedUrl)}`
  );
  return { props: {} };
}

export default Home;
