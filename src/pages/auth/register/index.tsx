import { AuthLayout, Register } from "components";
import React from "react";

export default function SignUp() {
  return (
    <AuthLayout title="Sign up" description="Create a new account">
      <Register />
    </AuthLayout>
  );
}
