import { AuthLayout, Login } from "components";
import React from "react";

interface IProps {
  redirect: string;
}

const SignIn: React.FC<IProps> = ({ redirect }) => {
  return (
    <AuthLayout title="Sign in" description="get started with our service">
      <Login redirect={redirect} />
    </AuthLayout>
  );
};

export default SignIn;
