import axios from "axios";
import { apiDomain } from "config/apiUrls";
import { getApiOptions } from "helper/api";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function proxyMiddleware(
  req: NextApiRequest,
  res: NextApiResponse<any>
) {
  const {
    method,
    body,
    query: { url },
  } = req;

  const cookies = req.cookies;

  if (!cookies?.sessionId) {
    res.status(401).json({ message: "Unauthorized" });
  }

  const opt = getApiOptions({
    method,
    url: `${apiDomain}/${url}`,
    sessionId: cookies.sessionId,
    ...(method !== "GET" && { data: body }),
  });

  try {
    const apiResponse = await axios.request(opt);
    res.status(200).json(apiResponse?.data);
  } catch (e: any) {
    const { status, statusText } = e.response;
    res.status(status).json({ message: statusText });
  }
}
