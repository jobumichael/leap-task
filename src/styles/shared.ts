import { css } from "styled-components";

export const ResponsiveWrapper = css`
  width: 90%;
  max-width: 1280px;
  margin-left: auto;
  margin-right: auto;
`;
