import { createGlobalStyle } from "styled-components";
import { device } from "./device";

export const GlobalStyle = createGlobalStyle`

*,
  *:after,
  *:before {
    box-sizing: border-box;
  }

  body {
    font-family: "DM Sans", sans-serif;
    background-color: #f2f5f7;
    line-height: 1.5;
    position: relative;
  }
html {
  font-size: 12px;
}
@media ${device.tablet} {
  html { font-size: 18px; }
}
@media ${device.desktop} {
  html { font-size: 25px; }
}

`;
