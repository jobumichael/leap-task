import { ArrowLeftOutlined } from "@ant-design/icons";
import { Button, Descriptions, Typography } from "antd";
import { IApplication } from "interfaces";
import React, { FC } from "react";
import * as S from "./Details.styles";

const { Title } = Typography;

interface IProps {
  setPageType: (pageType: string) => void;
  selectedApp: IApplication;
}

const Details: FC<IProps> = ({ selectedApp, setPageType }) => {
  const { name, secret, lang, version } = selectedApp;

  const handleBackClick = () => setPageType("list");

  return (
    <S.Container>
      <S.Wrapper>
        <Title level={3}>Application details</Title>
        <Descriptions
          title="Application Details"
          bordered
          layout="vertical"
          column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
        >
          <Descriptions.Item label="Name">{name}</Descriptions.Item>
          <Descriptions.Item label="Secret">{secret}</Descriptions.Item>
          <Descriptions.Item label="Language">{lang}</Descriptions.Item>
          <Descriptions.Item label="Version">{version}</Descriptions.Item>
        </Descriptions>
        <S.ActionsWrapper>
          <Button icon={<ArrowLeftOutlined />} onClick={handleBackClick}>
            Back
          </Button>
        </S.ActionsWrapper>
      </S.Wrapper>
    </S.Container>
  );
};

export default Details;
