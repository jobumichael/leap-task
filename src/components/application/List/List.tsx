import {
  DeleteOutlined,
  EditOutlined,
  ExclamationCircleOutlined,
  EyeOutlined,
} from "@ant-design/icons";
import { Button, message, Modal, Space, Spin, Table, Tag } from "antd";
import useFetch from "hooks/useFetch";
import { IApplication } from "interfaces";
import React, { FC, useEffect, useState } from "react";
import { deleteApplication } from "services/application";

const { confirm } = Modal;

interface IProps {
  setPageType: (pageType: string) => void;
  setSelectedApp: (app: IApplication | null) => void;
}

const List: FC<IProps> = ({ setPageType, setSelectedApp }) => {
  const [dataSource, setDataSource] = useState([]);

  const showPromiseConfirm = (id: string) => {
    confirm({
      title: "Do you want to delete these items?",
      icon: <ExclamationCircleOutlined />,
      async onOk() {
        await deleteApplication(id);
        const res = dataSource.filter((item: IApplication) => item.id !== id);
        setDataSource(res);
        message.success("Deleted successfully");
      },
      onCancel() {},
    });
  };

  const handleEditClick = (record: IApplication) => {
    setPageType("edit");
    setSelectedApp(record);
  };

  const handleDetailClick = (record: IApplication) => {
    setSelectedApp(record);
    setPageType("detail");
  };

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Secret",
      dataIndex: "secret",
      key: "secret",
    },
    {
      title: "Language",
      key: "lang",
      dataIndex: "lang",
    },
    {
      title: "Version",
      key: "version",
      dataIndex: "version",
      render: (version: string) => (
        <>
          <Tag color="green" key={version}>
            {version}
          </Tag>{" "}
        </>
      ),
    },
    {
      title: "Action",
      key: "id",
      render: (record: IApplication) => (
        <Space size="middle">
          <Button
            size="small"
            type="primary"
            onClick={() => handleDetailClick(record)}
            icon={<EyeOutlined />}
          >
            Detail
          </Button>
          <Button
            size="small"
            onClick={() => handleEditClick(record)}
            icon={<EditOutlined />}
          ></Button>
          <Button
            onClick={() => showPromiseConfirm(record.id)}
            size="small"
            danger
            type="primary"
            icon={<DeleteOutlined />}
          ></Button>
        </Space>
      ),
    },
  ];
  const { data, loading, error } = useFetch("applications");

  useEffect(() => {
    if (data) {
      setDataSource(data);
    }
  }, [data]);

  return (
    <Spin spinning={loading}>
      <div>
        <Table
          scroll={{ x: "200px" }}
          columns={columns}
          dataSource={dataSource}
        />
      </div>
    </Spin>
  );
};

export default List;
