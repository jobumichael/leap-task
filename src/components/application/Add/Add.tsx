import { Button, Form, Input, Typography } from "antd";
import { uuid } from "helper/uuid";
import { IApplication } from "interfaces";
import { FC } from "react";
import { postApplication, updateApplication } from "services/application";
import * as S from "./Add.styles";

const { Title } = Typography;

interface IProps {
  setPageType: (pageType: string) => void;
  selectedApp?: IApplication | null;
}

const AddApplication: FC<IProps> = ({ setPageType, selectedApp }) => {
  const isEditMode = selectedApp?.id !== undefined;

  const onFinish = async (values: any) => {
    const id = uuid();
    try {
      if (isEditMode) {
        await updateApplication({ id: selectedApp.id, ...values });
      } else {
        await postApplication({ id, ...values });
      }

      setPageType("list");
    } catch (error) {
      console.log(error);
    }
  };

  const handleCancelClick = () => setPageType("list");

  return (
    <S.Container>
      <S.Wrapper>
        <Title level={3}>{`${isEditMode ? "Edit" : "Add"} Application`}</Title>
        <Form
          name="basic"
          initialValues={{ ...selectedApp }}
          labelCol={{ span: 4 }}
          onFinish={onFinish}
          autoComplete="off"
        >
          <Form.Item
            label="Name"
            name="name"
            rules={[
              {
                required: true,
                message: "Please input your name!",
              },
            ]}
          >
            <Input size="large" />
          </Form.Item>

          <Form.Item
            label="Language"
            name="lang"
            rules={[
              {
                required: true,
                message: "Please input your language!",
              },
            ]}
          >
            <Input size="large" />
          </Form.Item>

          <Form.Item label="Secret" name="secret">
            <Input size="large" />
          </Form.Item>
          <Form.Item label="Version" name="version">
            <Input size="large" />
          </Form.Item>

          <S.ActionsWrapper>
            <Button size="large" type="primary" htmlType="submit">
              Submit
            </Button>
            <Button size="large" onClick={handleCancelClick}>
              Cancel
            </Button>
          </S.ActionsWrapper>
        </Form>
      </S.Wrapper>
    </S.Container>
  );
};

export default AddApplication;
