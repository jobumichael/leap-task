import styled from "styled-components";

export const Container = styled.div`
  padding: 30px;
  background-color: #fff;
  border-radius: 5px;
`;

export const Wrapper = styled.div`
  max-width: 500px;
  margin: 0 auto;

  h3 {
    text-align: center;
    margin-bottom: 30px;
  }
`;

export const ActionsWrapper = styled.div`
  text-align: right;

  button {
    margin-left: 5px;
  }
`;
