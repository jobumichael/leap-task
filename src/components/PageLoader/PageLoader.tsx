import { Spin } from "antd";
import React from "react";
import * as S from "./PageLoader.styles";

export default function PageLoader() {
  return (
    <Spin>
      <S.Wrapper></S.Wrapper>
    </Spin>
  );
}
