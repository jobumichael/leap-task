import { LoginOutlined, LogoutOutlined } from "@ant-design/icons";
import { Button } from "antd";
import { useAuth } from "context/auth";
import Link from "next/link";
import React from "react";
import * as S from "./Header.styles";

export default function Header() {
  const { user, isAuthenticated, logout, loading } = useAuth();

  if (loading) return null;

  return (
    <S.Header>
      <S.HederInner>
        <S.HeaderLogo>
          <Link href="/">
            <a>
              Leap
              <span> task</span>
            </a>
          </Link>
          {user && (
            <span>
              Welcome{" "}
              <span>{`${user.firstname || ""} ${user.lastname || ""}`}</span>
            </span>
          )}
        </S.HeaderLogo>
        <S.HeaderNavigation>
          {isAuthenticated ? (
            <>
              <Link href="/">
                <a>Dashboard</a>
              </Link>

              <a onClick={() => logout()}>
                <S.LogoutButton
                  type="primary"
                  size="large"
                  shape="round"
                  icon={<LogoutOutlined />}
                >
                  Logout
                </S.LogoutButton>
              </a>
            </>
          ) : (
            <>
              <S.AccountLabel>Already have an account?</S.AccountLabel>
              <Link href="/auth/login">
                <a>
                  <Button
                    size="large"
                    shape="round"
                    type="primary"
                    icon={<LoginOutlined />}
                  >
                    Login
                  </Button>
                </a>
              </Link>
              <Link href="/auth/register">
                <a>
                  <S.SignupButton shape="round" type="link">
                    Sign up
                  </S.SignupButton>
                </a>
              </Link>
            </>
          )}
        </S.HeaderNavigation>
      </S.HederInner>
    </S.Header>
  );
}
