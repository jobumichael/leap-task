import styled from "styled-components";

export const Wrapper = styled.div`
  margin-bottom: 1rem;

  .ant-breadcrumb-link {
    text-transform: capitalize;
  }
`;
