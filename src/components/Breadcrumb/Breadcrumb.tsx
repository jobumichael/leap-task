import { Breadcrumb } from "antd";
import React, { FC } from "react";
import * as S from "./Breadcrumb.styles";

interface IProps {
  pageType: string;
  setPageType: (pageType: string) => void;
}

const BreadcrumbCmp: FC<IProps> = ({ pageType, setPageType }) => {
  const handleHomeClick = () => setPageType("list");

  return (
    <S.Wrapper>
      <Breadcrumb>
        <Breadcrumb.Item>
          <a onClick={handleHomeClick}>Dashboard</a>
        </Breadcrumb.Item>
        <Breadcrumb.Item>{pageType}</Breadcrumb.Item>
      </Breadcrumb>
    </S.Wrapper>
  );
};

export default BreadcrumbCmp;
