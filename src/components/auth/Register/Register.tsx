import { LockOutlined, MailOutlined, UserOutlined } from "@ant-design/icons";
import { Alert, Button, Col, Form, Input, message, Row, Spin } from "antd";
import { useAuth } from "context/auth";
import { IUser } from "interfaces";
import Link from "next/link";
import router from "next/router";
import React, { useState } from "react";
import { register } from "services/auth";

export default function SignUp() {
  const [error, setError] = useState<any>();
  const [loading, setLoading] = useState(false);
  const { authenticate, user } = useAuth();

  const onFinish = async ({
    email,
    password,
    firstname,
    lastname,
    username,
  }: IUser) => {
    try {
      setLoading(true);
      await register({ username, password, email, firstname, lastname });
      message.success(
        "You have successfully registered. Please login to continue.",
        2
      );

      setTimeout(() => {
        router.push("/auth/login");
      }, 250);
    } catch ({ code, message }) {
      setError(message);
    } finally {
      setLoading(false);
    }
  };
  const handleFieldsChange = () => setError("");

  return (
    <Spin spinning={loading}>
      <Form
        name="register"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        layout="vertical"
        onFieldsChange={handleFieldsChange}
        style={{ maxWidth: "500px" }}
      >
        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              name="email"
              label="Email address"
              rules={[
                {
                  type: "email",
                  message: "The input is not valid E-mail!",
                },
                {
                  required: true,
                  message: "Please input your E-mail!",
                },
              ]}
              hasFeedback
            >
              <Input
                size="large"
                prefix={<MailOutlined />}
                placeholder="Email"
              />
            </Form.Item>
          </Col>

          <Col span={12}>
            <Form.Item
              name="username"
              label="Username"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
                {
                  pattern: new RegExp("^[a-z0-9_.]+$"),
                  message:
                    "The input is not valid Username! (lowercase, numbers, _ and . only)",
                },
              ]}
              hasFeedback
            >
              <Input
                size="large"
                prefix={<UserOutlined />}
                placeholder="Username"
              />
            </Form.Item>
          </Col>

          <Col span={12}>
            <Form.Item
              name="firstname"
              label="First name"
              rules={[
                {
                  required: true,
                  message: "Please input your firstname!",
                },
              ]}
              hasFeedback
            >
              <Input
                size="large"
                prefix={<UserOutlined />}
                placeholder="First name"
              />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label="Last name"
              name="lastname"
              rules={[
                {
                  required: true,
                  message: "Please input your lastname!",
                },
              ]}
              hasFeedback
            >
              <Input
                size="large"
                prefix={<UserOutlined />}
                placeholder="Last name"
              />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="password"
              label="Password"
              rules={[
                {
                  required: true,
                  message: "Please input your Password!",
                },
                {
                  min: 6,
                  message: "Password must be minimum 5 characters.",
                },
              ]}
              hasFeedback
            >
              <Input.Password
                size="large"
                prefix={<LockOutlined />}
                placeholder="Password"
              />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label="Confirm Password"
              name="confirm"
              dependencies={["password"]}
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "Please confirm your password!",
                },
                ({ getFieldValue }) => ({
                  validator(_, value) {
                    if (!value || getFieldValue("password") === value) {
                      return Promise.resolve();
                    }

                    return Promise.reject(
                      new Error(
                        "The two passwords that you entered do not match!"
                      )
                    );
                  },
                }),
              ]}
            >
              <Input.Password
                size="large"
                prefix={<LockOutlined />}
                placeholder="Confirm password"
              />
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col
            span={24}
            style={{
              textAlign: "center",
            }}
          >
            <Form.Item>
              <Button type="primary" htmlType="submit" size="large">
                Sign up
              </Button>
              <p style={{ marginTop: "12px" }}>
                {`Already have an account?  `}
                <Link href="/auth/login">
                  <a>Login now!</a>
                </Link>
              </p>
            </Form.Item>
            {
              <Form.Item
                style={
                  {
                    visibility: `${error ? "visible" : "hidden"}`,
                  } as React.CSSProperties
                }
              >
                <Alert message={error} type="error" showIcon />
              </Form.Item>
            }
          </Col>
        </Row>
      </Form>
    </Spin>
  );
}
