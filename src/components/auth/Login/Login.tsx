import { LockOutlined, MailOutlined } from "@ant-design/icons";
import { Alert, Button, Form, Input, Spin } from "antd";
import { useAuth } from "context/auth";
import Link from "next/link";
import React, { useState } from "react";
import { login } from "services/auth";

interface IProps {
  redirect?: string;
}

const SignIn: React.FC<IProps> = ({ redirect = "/" }) => {
  const [error, setError] = useState<any>("");
  const [loading, setLoading] = useState(false);
  const { authenticate } = useAuth();

  const onFinish = async ({ username, password }: any) => {
    try {
      setLoading(true);
      const result = await login({ username, password });

      await authenticate(result.data.session);
    } catch ({ code, message }) {
      setError(message);
    } finally {
      setLoading(false);
    }
  };

  const handleFieldsChange = () => setError("");

  return (
    <Spin spinning={loading}>
      <Form
        name="login"
        initialValues={{}}
        onFinish={onFinish}
        onFieldsChange={handleFieldsChange}
        layout="vertical"
        autoComplete="off"
      >
        <Form.Item
          name="username"
          label="Username"
          rules={[
            {
              required: true,
              message: "Please input your username!",
            },
          ]}
        >
          <Input
            size="large"
            prefix={<MailOutlined />}
            placeholder="Username"
          />
        </Form.Item>
        <Form.Item
          name="password"
          label="Password"
          rules={[
            {
              required: true,
              message: "Please input your Password!",
            },
          ]}
        >
          <Input
            prefix={<LockOutlined />}
            type="password"
            placeholder="Password"
            size="large"
          />
        </Form.Item>

        <Form.Item>
          <Button
            style={{ width: "100%" }}
            type="primary"
            size="large"
            htmlType="submit"
            loading={false}
          >
            Log in
          </Button>
          <p style={{ marginTop: "12px" }}>
            {`Don't have an account yet?`}
            <Link href="/auth/register">
              <a>Create one now!</a>
            </Link>
          </p>
        </Form.Item>
        {
          <Form.Item
            style={
              {
                visibility: `${error ? "visible" : "hidden"}`,
              } as React.CSSProperties
            }
          >
            <Alert message={error} type="error" showIcon />
          </Form.Item>
        }
      </Form>
    </Spin>
  );
};

export default SignIn;
