import styled from "styled-components";
import { ResponsiveWrapper } from "styles/shared";

export const HeaderLogo = styled.div`
  font-size: 2.25rem;
  font-weight: 600;

  a {
    color: #000;
  }
  span {
    color: ${(props) => props.theme.color.primary};
  }
`;

export const Main = styled.main`
  margin-top: 3rem;
  // For sticky header demo
  min-height: 150vh;

  .responsive-wrapper {
    width: 80%;
    max-width: 1280px;
    margin-left: auto;
    margin-right: auto;
  }
`;

export const Wrapper = styled.div`
  ${ResponsiveWrapper}
`;
