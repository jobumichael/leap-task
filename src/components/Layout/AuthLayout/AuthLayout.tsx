import { LockOutlined as Lock } from "@ant-design/icons";
import { Spin, Typography } from "antd";
import { Layout } from "components";
import { useAuth } from "context/auth";
import Head from "next/head";
import React, { FC } from "react";
import * as S from "./AuthLayout.styles";

const { Title, Text } = Typography;

interface IProps {
  children: React.ReactNode;
  title: string;
  description: string;
}

const AuthLayout: FC<IProps> = ({ title = "", description = "", children }) => {
  const { loading, user } = useAuth();

  if (loading || user) return <Spin />;

  return (
    <>
      <Head>
        <title>Leap task</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Layout>
        <S.Container>
          <S.Wrapper>
            {title && (
              <S.Title>
                <Lock />
                <Title level={3}>{title}</Title>
                <Text type="secondary">{description}</Text>
              </S.Title>
            )}

            {children}
          </S.Wrapper>
        </S.Container>
      </Layout>
    </>
  );
};

export default AuthLayout;
