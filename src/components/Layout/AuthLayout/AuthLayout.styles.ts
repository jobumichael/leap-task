import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  justify-content: center;
  align-items: center;
  padding-top: 30px;
`;
export const Wrapper = styled.div`
  padding: 30px;
  background-color: #fff;
  border-radius: 5px;
`;

export const Title = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-bottom: 50px;
  h3 {
    margin-top: 10px;
  }
`;
