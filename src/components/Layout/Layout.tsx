import Header from "components/Header/Header";
import React, { FC } from "react";
import * as S from "./Layout.styles";

interface IProps {
  title?: string;
  children: React.ReactNode;
}

const Layout: FC<IProps> = ({ children }) => {
  return (
    <>
      <Header />
      <S.Main>
        <S.Wrapper>{children}</S.Wrapper>
      </S.Main>
    </>
  );
};

export default Layout;
