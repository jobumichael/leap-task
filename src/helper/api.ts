import { AxiosRequestConfig } from "axios";

interface IGetOptions {
  method?: string;
  url: string;
  sessionId: string;
  data?: any;
}

export const getApiOptions = ({
  method = "GET",
  url,
  sessionId,
  data,
}: IGetOptions) => {
  const options = {
    method,
    url,
    headers: {
      cookie: `sessionId=${sessionId}`,
      "Content-Type": "application/json",
    },
    data,
  } as AxiosRequestConfig;

  return options;
};
